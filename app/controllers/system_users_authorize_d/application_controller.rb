class SystemUsersAuthorizeD::ApplicationController < Admin::ApplicationController
  layout "admin_auth"
  skip_before_filter :signed_in_system?, :check_access_rights
end