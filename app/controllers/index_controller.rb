class IndexController < ApplicationController

  def index
  end

  def rooms
    @rtc_room_id = params[:rtc_room_id]
    if @rtc_room_id != nil
      @room = RtcRoom.find_by(:alias => @rtc_room_id)
      if !@room.nil?
      else
        redirect_to root_path
      end
    else
      redirect_to root_path
    end
  end

  def ajax_get_room_count
    count = RtcRoom.find_by(:alias => params[:room_name]).block_count
    render :text => count
  end

  def ajax_add_new_user
    user = User.new
    user.name = params[:name]
    user.easyrtcid = params[:easyrtcid]
    user.save
    render :text => "Success"
  end

  def ajax_delete_new_user
    user = User.find_by(:easyrtcid => params[:easyrtcid])
    if (!user.nil?)
      user.destroy
    end
    render :text => "Success"
  end

  def ajax_get_user_name_by_easyrtcid
    user = User.where(:easyrtcid => params[:easyrtcid]).last()
    render :text => user.name
  end

end
