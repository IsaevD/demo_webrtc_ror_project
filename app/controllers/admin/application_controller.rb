class Admin::ApplicationController < ActionController::Base
  include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include AccessRightsD::ApplicationHelper
  include SystemUserActionsLogD::ApplicationHelper
  layout "admin"
  helper ObjectGeneratorD::ApplicationHelper
  helper AuthorizeD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  helper AccessRightsD::ApplicationHelper
  helper SystemUserActionsLogD::ApplicationHelper

  before_filter :init_app_variables, :signed_in_system?

  def signed_in_system?
    signed_in_user(system_users_authorize_d.sign_in_path, current_system_user)
  end


  private

    def init_app_variables
      items = params[:controller].split("/")
      @module_name = items[0]
      @module_item_alias = items[1]
      @module_item = ModulesD::ModuleItem.find_by(:module_name => @module_name, :alias => @module_item_alias)
      if signed_in(current_system_user)
        @menu_items = {
            :system_users => {
                :type => :url,
                :icon => "icon-user",
                :label => "Администраторы",
                :value => system_main_items_d.system_users_path
            },
            :rooms => {
                :type => :url,
                :icon => "icon-home",
                :label => "Комнаты",
                :value => Rails.application.routes.url_helpers.rtc_rooms_path
            },
            :logout => {
                :type => :link,
                :icon => "icon-logout",
                :label => t("labels.menu.logout"),
                :value => generate_sign_out_link(current_system_user, system_users_authorize_d.sign_out_path)
            }
        }
      end

    end


end
