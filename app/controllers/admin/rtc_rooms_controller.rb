class Admin::RtcRoomsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def start_room
  end

  def stop_room
  end

  private

    def init_variables

      @object = {
          :name => "rtc_rooms",
          :entity => RtcRoom,
          :param_name => :rtc_room,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.rtc_rooms_path,
              :new_path => Rails.application.routes.url_helpers.new_rtc_room_path,
              :edit_path => Rails.application.routes.url_helpers.rtc_rooms_path
          },
          :fields => {
              :alias => {
                  :type => :string,
                  :label => t('form.labels.alias'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true
              },
              :block_count => {
                  :type => :string,
                  :label => "Максимальное количество участников",
                  :show_in_card => true,
                  :show_in_table => false,
                  :help_message => "При превышении количество участников новые участники не смогут зайти в комнату"
              },
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end

end
