// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require visual_interfaces_d/application
//= require socket.io
//= require easyrtc
//= require_tree .

var main_easy_rtc_url = "//185.31.160.244:8080";

$(function(){
    initializeStartRoom();
    initializeStopRoom();
    initializeJoinRoomForm();
    intializeExitButton();
    initializeSendMessage();
    initShareDesktop();
    //initialVisualInterface();
    $("#fullsize").click(function(){
        $(".web_cams").toggleClass("fullsize_webcams");
    });
    $( document ).on("click", ".web_cams .icon-zoom", function(){
        $(".web_cams").toggleClass("fullsize_webcams").toggleClass("change_visible");
        $(this).parent().parent().parent().toggleClass("active");
    });
    $( document ).on("click", ".presentation .icon-zoom", function(){
        $(".presentation").toggleClass("fullsize_webcams").toggleClass("change_visible");
        $(this).parent().parent().parent().toggleClass("active");
    });

});


function initShareDesktop() {

    var iframeUrl =  'https://www.webrtc-experiment.com/getSourceId/';

    easyrtc.initDesktopStream= function(successCallback, failureCallback, streamName) {

        if (!!navigator.mozGetUserMedia) {
            easyrtc._presetMediaConstraints = {
                video: {
                    mozMediaSource: 'window',
                    mediaSource: 'window',
                    maxWidth: 1920,
                    maxHeight: 1080,
                    minAspectRatio: 1.77
                },
                audio: false
            };
            easyrtc.initMediaSource(successCallback, failureCallback, streamName);
            return;
        }

        postMessage();

        var cb = function(event) {
            if (!event.data) return;

            if (event.data.chromeMediaSourceId) {
                window.removeEventListener("message", cb);
                if (event.data.chromeMediaSourceId === 'PermissionDeniedError') {
                    failureCallback(easyrtc.errCodes.MEDIA_ERR, 'permission-denied');
                } else {
                    easyrtc._presetMediaConstraints = {
                        video: {
                            mandatory: {
                                chromeMediaSource:'desktop',
                                chromeMediaSourceId: event.data.chromeMediaSourceId,
                                maxWidth: 1920,
                                maxHeight: 1080,
                                minAspectRatio: 1.77
                            }
                        },
                        audio: false
                    }
                    easyrtc.initMediaSource(successCallback, failureCallback, streamName);
                }
            }

            if (event.data.chromeExtensionStatus) {
                console.log("extension status is ", event.data.chromeExtensionStatus);
            }
        };
        easyrtc.desktopCaptureInstalled = null;
        window.addEventListener('message', cb);
    };


    var iframe = document.createElement('iframe');

    function postMessage() {
        if (!iframe.isLoaded) {
            setTimeout(postMessage, 100);
            return;
        }

        iframe.contentWindow.postMessage({
            captureSourceId: true
        }, '*');
    }

    iframe.onload = function() {
        iframe.isLoaded = true;
    };

    iframe.src = iframeUrl;

    iframe.style.display = 'none';
    (document.body || document.documentElement).appendChild(iframe);

}



function initializeStartRoom() {
    $("#start_room").click(function(){
        connectToRTCServer(main_easy_rtc_url);
        joinRoom($(this).data("alias"));
        return false;
    });
}
function initializeStopRoom() {
    $("#stop_room").click(function(){
        leaveRoom($(this).data("alias"));
        return false;
    });
}
function initializeJoinRoomForm() {
    $("#enter").submit(function(){
        var alias = $(this).children("#room_alias").val();
        connectToRTCServer(main_easy_rtc_url);
        alias = getParameterByName("rtc_room_id");
        joinRoom(alias);
        return false;
    });
}
function intializeExitButton() {
    $(".icon-exit").click(function(){
        leaveRoom($(this).data("alias"));
        $(".interface").hide();
        $(".messages li").remove();
        $(".popup").show();
    });
}
function initializeSendMessage() {
    $(".chat form").submit(function() {
        sendMessage($(".icon-exit").data("alias"), $(this).children("textarea").val());
        $(".messages").scrollTop($(".messages ul").height());
        return false;
    });
    $('.chat textarea').keydown(function(event) {
        if (event.keyCode == 13) {
            $(".chat form").submit();
            $('.chat textarea').val("");
            return false;
        }
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateS(stream) {
    alert(stream);
    document.querySelector('video#myvideo').src = stream.url;
}

function connectToRTCServer(easy_rtc_url) {
    easyrtc.setSocketUrl(easy_rtc_url);
    easyrtc.setPeerListener(addMessageToChat);
    easyrtc.setRoomOccupantListener(refreshUsersList);
    //easyrtc.enableVideo(false);
    //easyrtc.enableVideoReceive(false);
    /*
    easyrtc.enableAudio(true);
    easyrtc.enableVideo(true);
    easyrtc.initMediaSource(
        function(){
            easyrtc.connect("easyrtc_app", loginSuccess, loginFailure);
        },
        function(errorCode, errmesg){
            easyrtc.showError(errorCode, errmesg);
        }
    );
    */
}

function joinRoom(room_alias){
    easyrtc.joinRoom(room_alias, null,
        function(roomname) {
        },
        function(errorCode, errorText, roomName) {
            easyrtc.showError(errorCode, errorText + ": room name was(" + roomName + ")");
        }
    );
    /*
    easyrtc.easyApp("easyrtc.joinTo"+room_alias, null, null, function(){
        addNewUser($("#enter #name").val(), easyrtc.myEasyrtcid);
        $(".interface").show();
        $(".popup").hide();
    });
    */
    easyrtc.enableAudio(true);
    easyrtc.enableVideo(true);
    easyrtc.initMediaSource(
        function(){
        },
        function(errorCode, errmesg){
            easyrtc.showError(errorCode, errmesg);
        }
    );


    easyrtc.connect("easyrtc.joinTo"+room_alias,
        function(){
            addNewUser($("#enter #name").val(), easyrtc.myEasyrtcid);
            $(".interface").show();
            $(".popup").hide();
        }, function(){alert("no");});

    /*
        easyrtc.easyApp("easyrtc.test");

    easyrtc.setOnCall( function(easyrtcid, slot) {
        $(".users table").append("<tr><td>" + getUserName(easyrtcid) + "</td></tr>");
    });
    */

}
function leaveRoom(room_alias){
    easyrtc.leaveRoom(room_alias, null,
        function() {
            /* we'll geta room entry event for the room we were actually added to */
        },
        function(errorCode, errorText, roomName) {
            easyrtc.showError(errorCode, errorText + ": room name was(" + roomName + ")");
        }
    );
    removeUser(easyrtc.myEasyrtcid);
    easyrtc.disconnect();
}

// Отправка сообщений
function sendMessage(destRoom, text) {
    if (replaceSpecialSymbols(text).length === 0) { // Don't send just whitespace
        return;
    }
    var dest = {};
    dest.targetRoom = destRoom;
    if( text === "empty")
        easyrtc.sendPeerMessage(dest, "message");
    else {
        easyrtc.sendDataWS(dest, "message", text, function (reply) {
            console.log(reply.msgData.errorText);
            if (reply.msgType === "error") {
                easyrtc.showError(reply.msgData.errorCode, reply.msgData.errorText);
            }
        });
    }
    addMessageToChat("Me", "message", text);
}
function replaceSpecialSymbols(string) {
    return string
        .replace(/&/g,"&amp;")
        .replace(/</g,"&lt;")
        .replace(/>/g,"&gt;")
        .replace(/\n/g, "<br />")
        .replace(/\s/g, "");
}
function addMessageToChat(who, msgType, content) {
    if (who != "Me") {
        who = getUserName(who);
    }

    var currentdate = new Date();
    var datetime = " " + currentdate.getDate() + "."
        + (currentdate.getMonth()+1)  + "."
        + currentdate.getFullYear() + "  "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
    $(".messages ul").append("<li><div class='header_message'><span class='name'>"+who+"</span><span class='date'>"+datetime+"</span></div><div class='content_block'>"+content+"</div></li>");
}

var previous_occupants = [];

// Обновление списк пользователей
function refreshUsersList(roomName, occupants, isPrimary) {

    //easyrtc.setRoomOccupantListener(null);

    if ($("#myvideo").length == 0) {
        $(".users table").append("<tr id='users_" + easyrtc.myEasyrtcid + "'><td>Me ( " + getUserName(easyrtc.myEasyrtcid) + " )</td></tr>");
        $(".web_cams ul").append("<li><div class='frame'><video autoplay id='myvideo'></video><div class='header_block'>My video<div class='icon icon-zoom'></div></div></div></li>");
        navigator.webkitGetUserMedia(
                {video:true}, // тип запрашиваемого стрима (может быть audio)
                function(stream) {
                    videoStreamUrl = window.URL.createObjectURL(stream);
                    $("#myvideo").attr("src", videoStreamUrl);
                },
                function(){/*callback в случае отказа*/})
    }


    var list = [];
    var connectCount = 0;
    for(var easyrtcid in occupants ) {
        list.push(easyrtcid);
        index = previous_occupants.indexOf(easyrtcid);
        if (index != -1) {
            previous_occupants.splice(index, 1);
        }
    }


    for(var i in previous_occupants) {
        $(".users table #users_"+previous_occupants[i]).remove();
        $(".users table #video_"+previous_occupants[i]).parent().parent().remove();
        $(".users table #desktop_"+previous_occupants[i]).parent().parent().remove();
    }
    previous_occupants = list;

    function establishConnection(position) {
        function callSuccess() {
            connectCount++;
            if( connectCount < 10 && position > 0) {
                establishConnection(position-1);
            }
        }
        function callFailure(errorCode, errorText) {
            easyrtc.showError(errorCode, errorText);
            if( connectCount < 10 && position > 0) {
                establishConnection(position-1);
            }
        }
        easyrtcid = list[position];
        if ($(".users table #users_"+easyrtcid).length == 0) {
            var name = getUserName(easyrtcid);
            if (name != undefined) {
                easyrtc.initDesktopStream(
                    function(stream) {
                        $(".present_block ul").append("<li><div class='frame'><video muted='true' autoplay='true' id='desktop_" + easyrtcid + "'></video><div class='header_block'>" + getUserName(easyrtcid) + "<div class='icon icon-zoom'></div></div></div></li>");
                        var video = document.getElementById("desktop_"+easyrtcid);
                        easyrtc.setVideoObjectSrc(video, stream);
                        easyrtc.addStreamToCall(easyrtcid, stream);
                    },
                    function(errCode, errText) {
                        console.log(errText);
                    },
                    easyrtcid);
                $(".users table").append("<tr id='users_" + easyrtcid + "'><td>" + getUserName(easyrtcid) + "</td></tr>");
                $(".web_cams ul").append("<li><div class='frame'><video id='video_" + easyrtcid + "'></video><div class='header_block'>" + getUserName(easyrtcid) + "<div class='icon icon-zoom'></div></div></div></li>");
                easyrtc.call(list[position], callSuccess, callFailure);
            }
        }
    }

    if( list.length > 0) {
        establishConnection(list.length-1);
    }

    /*
    $(".users table tr").remove();
    for(var easyrtcid in occupants) {
        $(".users table").append("<tr><td>" + getUserName(easyrtcid) + "</td></tr>");
        call(easyrtcid);
    }
    */
}



// Звонок пользователю
function call(otherEasyrtcid) {
    easyrtc.hangupAll();
    var acceptedCB = function(accepted, caller) {
        if( !accepted ) {
            easyrtc.showError("CALL-REJECTED", "Sorry, your call to " + easyrtc.idToName(caller) + " was rejected");
        }
    };
    var successCB = function() {
    };
    var failureCB = function() {
    };
    easyrtc.call(otherEasyrtcid, successCB, failureCB, acceptedCB);
}



easyrtc.setStreamAcceptor( function(easyrtcid, stream, streamName) {
    video = document.getElementById("video_" + easyrtcid);
    if (video.attributes.src == undefined) {
        easyrtc.setVideoObjectSrc(video, stream);
    }
});
easyrtc.setOnStreamClosed( function (easyrtcid) {
});


function checkUserCountInRoom(roomName) {
    var count = 1;
    $.ajax({
        type: "POST",
        async: false,
        data: { room_name: roomName },
        url: "ajax_get_room_count",
        success: function (data) {
            count = parseInt(data);
        }
    });
    var userCount = easyrtc.getRoomOccupantsAsArray(roomName).length;
    if (userCount <= count)
        return true;
    else
        return false;
}

function addNewUser(name, easyrtcid) {
    $.ajax({
        type: "POST",
        async: false,
        data: { name: name, easyrtcid: easyrtcid },
        url: "ajax_add_new_user",
        success: function (data) {
        }
    });
}

function removeUser(easyrtcid) {
    $.ajax({
        type: "POST",
        async: false,
        data: { easyrtcid: easyrtcid },
        url: "ajax_delete_new_user",
        success: function (data) {
        }
    });
}

function getUserName(easyrtcid) {
    var name = "!Ошибка!";
    $.ajax({
        type: "POST",
        async: false,
        data: { easyrtcid: easyrtcid },
        url: "ajax_get_user_name_by_easyrtcid",
        success: function (data) {
            name = data;
        }
    });
    return name;
}

//= require turbolinks
