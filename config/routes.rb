Rails.application.routes.draw do

  root "index#index"

  match "/rooms", to: "index#rooms", via: "get"
  match '/ajax_get_room_count', to: "index#ajax_get_room_count" , via: "post"
  match '/ajax_add_new_user', to: "index#ajax_add_new_user" , via: "post"
  match '/ajax_delete_new_user', to: "index#ajax_delete_new_user" , via: "post"
  match '/ajax_get_user_name_by_easyrtcid', to: "index#ajax_get_user_name_by_easyrtcid" , via: "post"


  scope "/admin" do
    mount ModulesD::Engine => "/"
    mount SystemMainItemsD::Engine => "/"
    mount SystemUsersAuthorizeD::Engine => "/"
    mount AccessRightsD::Engine => "/"
    mount SystemUserActionsLogD::Engine => "/"
    scope module: "admin" do
      resources :rtc_rooms
      match "/start_room", to: "rtc_rooms#start_room", via: "get"
      match "/stop_room", to: "rtc_rooms#stop_room", via: "get"
    end
  end



end
