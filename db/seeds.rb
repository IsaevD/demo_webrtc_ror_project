ModulesD::Engine.load_seed
SystemMainItemsD::Engine.load_seed
AccessRightsD::Engine.load_seed
SystemUserActionsLogD::Engine.load_seed

# Импорт информации о модулях

modules = {
    "rtc_rooms" => {
        :module_name => "admin",
        :name => "Комнаты",
        :description => "Предназначен для управления комнатами",
        :git_url => ""
    }
}
modules.each do |key, value|
  if (ModulesD::ModuleItem.find_by(:alias => key).nil?)
    ModulesD::ModuleItem.new({ :alias => key, :module_name => value[:module_name], :name => value[:name], :description => value[:description], :git_url => value[:git_url] }).save
  end
end
puts "Custom modules import success"