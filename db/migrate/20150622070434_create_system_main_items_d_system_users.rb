class CreateSystemMainItemsDSystemUsers < ActiveRecord::Migration
  def change
    create_table :system_main_items_d_system_users do |t|

      t.string :login
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :remember_token
      t.integer :system_user_role_id
      t.integer :system_state_id

      t.timestamps
    end
  end
end
