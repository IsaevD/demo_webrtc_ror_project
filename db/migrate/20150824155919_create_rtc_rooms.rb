class CreateRtcRooms < ActiveRecord::Migration
  def change
    create_table :rtc_rooms do |t|
      t.string :alias
      t.string :name
      t.integer :block_count
      t.timestamps
    end
  end
end
