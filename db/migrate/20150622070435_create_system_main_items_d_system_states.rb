class CreateSystemMainItemsDSystemStates < ActiveRecord::Migration
  def change
    create_table :system_main_items_d_system_states do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
