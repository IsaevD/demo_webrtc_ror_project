class CreateSystemUserActionsLogDLogRecordStates < ActiveRecord::Migration
  def change
    create_table :system_user_actions_log_d_log_record_states do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
