class CreateAccessRightsDAccessRights < ActiveRecord::Migration
  def change
    create_table :access_rights_d_access_rights do |t|
      t.integer :system_user_role_id

      t.timestamps
    end
  end
end
